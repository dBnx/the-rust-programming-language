use std::fs;
use std::error::Error;

mod config;
pub use config::Config;

pub fn run( config: Config ) -> Result<(), Box<dyn Error>> { 
    let file = fs::read_to_string( config.filename )?;

    let results = if config.case_sensitive { 
        search( &config.query, &file ) 
    } else { 
        search_case_insensitive( &config.query, &file ) 
    };

    for line in results {
        println!( "{}", line );
    }

    Ok(())
}


pub fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let mut ret = Vec::new();
    for line in contents.lines() {
        if line.contains( query ) {
            ret.push( line );
        }
    }
    ret
}

pub fn search_case_insensitive<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
    let query = query.to_lowercase();
    let mut ret = Vec::new();
    for line in contents.lines() {
        if line.to_lowercase().contains( &query ) {
            ret.push( line );
        }
    }
    ret
}

#[cfg(test)]
mod tests {
    use super::*;
    
    #[test]
    fn search_case_sensitive() {
        let query = "duct";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape is useful!";

        assert_eq!(vec!["safe, fast, productive."], search(query, contents));
    }

    #[test]
    fn search_case_insensitive_text() {
        let query = "DuCt";
        let contents = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape is useful!";

        assert_eq!(vec!["safe, fast, productive.", "Duct tape is useful!"], search_case_insensitive(query, contents));
    }
}
