use std::env;

#[derive(Debug)]
pub struct Config<'a> {
    pub query: &'a str,
    pub filename: &'a str,
    pub case_sensitive: bool,
}

impl<'a> Config<'a> {
    pub fn new( args: &'a [String] ) -> Result<Config<'a>, &'static str> {
        if args.len() != 3 {
            Err("Not enough arguments. 'query' and 'filename' needed.")
        }
        else
        {
            let case_sensitive = env::var("CASE_INSENSITIVE").is_err();
            Ok(Config {
                query: &args[1], 
                filename: &args[2],
                case_sensitive,
            })
        }
    }
}

// TODO: Change panic based function to Result<>
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn config_new_with_sane_input() {
        let args = vec![  "a".to_string(), "b".to_string(), "c".to_string() ];
        let config = Config::new( &args ).unwrap();
        assert_eq!( "b", config.query );
        assert_eq!( "c", config.filename );
    }

    #[test]
    #[should_panic(expected = "Not enough")]
    fn config_new_without_enough_arguments() {
        let args = vec![  "a".to_string(), "b".to_string() ];
        Config::new( &args ).unwrap();
    }

    #[test]
    #[should_panic(expected = "Not enough")]
    fn config_new_too_much_arguments() {
        let args = vec![  "a".to_string(), "b".to_string(), "c".to_string(), "d".to_string() ];
        Config::new( &args ).unwrap();
    }
}
