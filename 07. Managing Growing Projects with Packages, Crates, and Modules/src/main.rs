

// Simplify:
//  use std::io;
//  use std::io::Write;
// to
//  use std::io::{self, Write};
// using nested paths.

use packages_crates_and_modules;

fn main() {
    println!("{}", packages_crates_and_modules::description() );

    packages_crates_and_modules::eat_at_restaurant();
    packages_crates_and_modules::hosting::seat_at_table(); // -> lib.rs
    packages_crates_and_modules::serving::take_payment();  // -> front_of_house_2.rs -> front_of_house_2/serving.rs
}
