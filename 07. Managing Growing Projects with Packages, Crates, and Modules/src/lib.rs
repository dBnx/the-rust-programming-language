
mod front_of_house_2;

mod front_of_house {
    pub mod hosting {
        pub fn add_to_waitlist() {}

        pub fn seat_at_table() { internal_function_which_is_not_publically_visible(); }

        fn internal_function_which_is_not_publically_visible() {}
    }

    // Reexports content of front_of_house_2::serving 
    pub use super::front_of_house_2::serving;
}

pub use self::front_of_house::{hosting, serving};

pub fn eat_at_restaurant() {
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
    hosting::add_to_waitlist();
}

pub fn description() -> &'static str {
"There can be either 0 or 1 library: src/lib.rs
Which has the name of the project and can be 'use'd in binary packages.

There can be any number of binaries, which are all located in src/bin/ .
If and only if there is one binary package, it can be named src/main.rs, which
generates a binary with the name of the project.

There has to be at least one binary or library in a project."
}
