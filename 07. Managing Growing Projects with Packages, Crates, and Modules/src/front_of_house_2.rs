
// Resides in src/front_of_house_2/serving.rs
//
//pub mod serving {
//    pub fn take_order() {}
//
//    pub fn serve_order() {}
//
//    pub fn take_payment() {}
//}
//
// """Using a semicolon after mod front_of_house rather than using a block tells Rust to load 
// the contents of the module from another file with the same name as the module."""
pub mod serving;
