
use webserver;
use webserver::PageData::{File,Html};

const HTML_ROOT: &str = r#"<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Hello!</title>
  </head>
  <body>
    <h1>Hello!</h1>
    <p>Hi from Rust</p>
  </body>
</html>"#;

const HTML_404: &str = r#"<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>ERROR</title>
  </head>
  <body>
    <h1>Error</h1>
    <p>Invalid request.</p>
  </body>
</html>"#;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let html_root: String = HTML_ROOT.to_string();
    let html_404 = HTML_404.to_string();

    let location = "127.0.0.1:7878";
    println!("To view the webpage connect to: {}", location);

    let page_map: webserver::WebpageMap = vec![
        ("/".to_string(), Html( html_root ) ),
        ("/slow".to_string(), File( "pages/slow.html".to_string() ) ),
        ("/invalid".to_string(), File( "pages/invalid.html".to_string() ) ),
    ].into_iter().collect();

    let page404 = Html( html_404 );

    webserver::run( location, page_map, page404 )
}

