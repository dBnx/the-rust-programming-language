 
use std::io::prelude::*;
use std::net::TcpListener;
use std::net::TcpStream;

use std::fs;
use std::collections::HashMap;
use std::sync::Arc;
use std::sync::mpsc::{self,Receiver};

mod thread_pool;
use thread_pool::ThreadPool;

/// Universal datastructure to refer to HTML page.
///
/// # Examples
///
/// ```
/// use webserver::PageData::{Html, File};
/// const HTML_404: &str = r#"<!DOCTYPE html>
///<html lang="en">
///  <head>
///    <meta charset="utf-8">
///    <title>ERROR</title>
///  </head>
///  <body>
///    <h1>Error</h1>
///    <p>Invalid request.</p>
///  </body>
///</html>"#;
///
/// let html_404 = HTML_404.to_string();
/// let page404 = Html( html_404 );
/// let page404 = File( "pages/root.html".to_string() );
/// ```
pub enum PageData {
    /// Includes an path to a file, which is served as is. Does not buffer the file's content.
    File(String),
    /// Contains HTML, which can be hardcoded or read using another method. This option is 
    /// preferred if some kind of performance is expected as `File` does not buffer it's content.
    Html(String),
}

impl PageData {
    // TODO: Change to &str and make File buffering
    fn get_html(&self) -> Option<String> {
        match self {
            PageData::Html(html) => Some( html.to_string() ),
            PageData::File(path) => match fs::read_to_string(path) {
                Ok( string ) => Some( string ),
                _ => None,
            },
        }
    }
}

/// Defines how the different files or inline HTML maps to the website paths. 
///
/// # Examples
///
/// ```
/// use webserver::PageData::File;
/// use webserver::WebpageMap;
///
/// let pages: webserver::WebpageMap = vec![
///     ("/".to_string(), File( "pages/root.html".to_string() ) ),
///     ("/bob".to_string(), File( "pages/bob.html".to_string() ) ),
///     ("/alice".to_string(), File( "pages/alice.html".to_string() ) ),
///     ].into_iter().collect();
/// ```
pub type WebpageMap = HashMap<String, PageData>;

/// Starts webserver and serves pages according to `pages` und uses a thread pool of 6 threads.
///
/// # Arguments
///
/// * `location` - IP and Port to use.
/// * `pages` - A `WebpageMap` which maps paths to corresponding `PageData`s. An entry for 
///   the root document is required.
/// * `page404` - Special page to use if there is an 404 error.
///
/// # Examples
///
/// ```
/// use webserver::WebpageMap;
/// use webserver::PageData::{self, File};
///
/// let pages: webserver::WebpageMap = vec![("/".to_string(), File( "pages/root.html".to_string() ) )].into_iter().collect();
/// let page404: PageData = File( "pages/404.html".to_string() );
///
/// webserver::run( "127.0.0.1:7878", pages, page404 ).unwrap();
/// ```
pub fn run( location: &str, pages: WebpageMap, page404: PageData ) -> Result<(), Box<dyn std::error::Error>> {
    let ctrlc_tx = setup_ctrlc_channel(); 
    let listener = TcpListener::bind( location )?;
    let pages = Arc::from( pages );
    let page404 = Arc::from( page404 );
    

    let thread_pool = ThreadPool::new( 6 );
    //for stream in listener.incoming() {
    //    let stream = stream?;
    //    let pages = Arc::clone( &pages ); let page404 = Arc::clone( &page404 );
    //    thread_pool.add_job( move || {
    //        handle_connection( stream, &pages, &page404 );
    //    });
    //    if let Ok(()) = ctrlc_tx.try_recv() { break; }
    //}
    listener.set_nonblocking(true).expect("Cannot set non-blocking");

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                let pages = Arc::clone( &pages ); let page404 = Arc::clone( &page404 );
                thread_pool.add_job( move || {
                    handle_connection( stream, &pages, &page404 );
                });
            }
            Err(ref e) if e.kind() == std::io::ErrorKind::WouldBlock => {
                if let Ok(()) = ctrlc_tx.try_recv() { break; }
                // wait until network socket is ready, typically implemented
                // via platform-specific APIs such as epoll or IOCP
                //wait_for_fd();
                std::thread::sleep(std::time::Duration::from_micros(500));
                continue;
            }
            Err(e) => panic!("encountered IO error: {}", e),
        }
    }
    Ok( () )
}

fn handle_connection( mut stream: TcpStream, pages: &WebpageMap, page404: &PageData ) {
    let mut buffer = [0; 1024];
    stream.read( &mut buffer ).unwrap();
    let buffer = String::from_utf8_lossy( &buffer );

    let response = if let Some(path) = get_html_get_request_path( &buffer ) {
        eprintln!( "path = {:#?}", path );
        if path.contains("slow") { std::thread::sleep( std::time::Duration::from_secs(5)); }

        let html = pages.get( path ).unwrap_or( page404 )
            .get_html().unwrap_or( String::from("This file should exist ...") );
        construct_html_response( 200, &html )
    } else {
        let html = page404.get_html().unwrap();
        construct_html_response( 404, &html )
    };
    stream.write( response.as_bytes() ).unwrap();
    stream.flush().unwrap();
}

fn get_html_get_request_path( request: &str ) -> Option<&str> {
    if request.starts_with("GET") {
        let start = request.find("/")?;
        let end = start + request[start..].find(" ")?;
        Some( &request[start..end] )
    }
    else {
        None
    }
}

fn construct_html_response( statuscode: i32, html: &str ) -> String {
    let header = format!("Content-Length: {}", html.len() );
    construct_basic_html_response( statuscode, &header, html )
}

fn construct_basic_html_response( statuscode: i32, header: &str, body: &str ) -> String {
    let version = "HTTP/1.1";
    let reason_phrase = match statuscode {
        200 => "OK",
        404 => "NOT FOUND",
        _ => panic!("Invalid statuscode. Reason phrase for this code is not implemented"),
    };
    format!("{} {} {}\r\n{}\r\n\r\n{}", version, statuscode, reason_phrase, header, body )
}

fn setup_ctrlc_channel() -> Receiver<()> {
    let (tx, rx) = mpsc::channel();
    ctrlc::set_handler(move || {
        println!("");
        tx.send(()).expect("[ERROR] Unable to send ctrl-c event over mpsc channel.");
    }).expect("[ERROR] Unable to register signal handler.");
    rx
}
