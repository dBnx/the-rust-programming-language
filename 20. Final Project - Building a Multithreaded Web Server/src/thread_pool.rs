
use std::thread;
use std::sync::{Arc,Mutex};
use std::sync::mpsc::{self,Sender,Receiver};

type Job = Box<dyn FnOnce() + Send + 'static>;

enum Message {
    NewJob(Job),
    Terminate,
}

pub struct ThreadPool {
    worker: Vec<Worker>,
    job_tx: Sender<Message>,
}

impl ThreadPool {
    /// Create a new ThreadPool
    ///
    /// num_threads is the number of hardware threads spawned. If it is equal to 0 
    /// everything is directly executed in the calling thread.
    pub fn new( num_threads: usize ) -> ThreadPool {
        let (tx, rx) = mpsc::channel();
        let rx = Arc::new(Mutex::from(rx));

        let mut worker = Vec::with_capacity( num_threads );
        for i in 0..num_threads {
            worker.push( Worker::new( i, Arc::clone(&rx) ) );
        }

        ThreadPool{ worker, job_tx: tx }
    }

    pub fn add_job<F>( &self, f: F )
    where 
        F: FnOnce() -> (),
        F: Send + 'static, // static because the runtime is not bounded
    {
        let job = Box::new( f );
        self.job_tx.send( Message::NewJob( job ) ).unwrap();
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        eprintln!("[THREADPOOL] Sending termination messages to workers.");
        for _ in &mut self.worker {
            self.job_tx.send( Message::Terminate ).unwrap();
        }
        //Reduntant. Happens when the Vec<Worker> is dropped. 
        // for w in &mut self.worker { std::mem::drop(w); }   
    }
}

struct Worker {
    id: usize,
    thread: Option<thread::JoinHandle<()>>,
}

impl Worker {
    fn new( id: usize, rx: Arc< Mutex< Receiver<Message> > > ) -> Worker {
        // while let Ok(job) = receiver.lock().unwrap().recv() { ... }
        // does not work, because the lock would live as long as the scope of the while
        // and would therefore lock until the job is completed.
        let thread = thread::spawn(move || loop {
            match rx.lock().unwrap().recv().unwrap() {
                Message::NewJob(job) => {
                    println!("Worker {} got a job; executing.", id);
                    job();
                },
                Message::Terminate => break,
            }
        });

        Worker{ id, thread: Some(thread) }
    }
}

impl Drop for Worker {
    fn drop(&mut self) {
        if let Some(thread) = self.thread.take() {
            // take() takes ownership and replaces the Option with None
            thread.join().unwrap(); 
        }
        eprintln!("[THREADPOOL][WORKER] Shutting down worker {}", self.id);
    }
}
