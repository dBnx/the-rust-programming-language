
const UNCHANGEABLE_VALUE : f32 = 1.2;

fn foo( x: i32 ) -> i32 {
    if x == 42 { 0 }
    else { x }
}

fn main() {
    println!( "Unchangeable value is {}", UNCHANGEABLE_VALUE );

    // Tuples: Fixed width and types
    let x: (i32, f64, u8) = (500, 6.4, 1);
    let five_hundred = x.0;
    let six_point_four = x.1;
    let one = x.2;

    // Arrays: Fixed width, one type, on stack
    let a = [1, 2, 3, 4, 5];
    let first = a[0];
    let second = a[1];

    let ones = [3; 5];

    let g = foo( five_hundred );
    let h = if one == 42 { 0 } else { one };

    // Loops: loop for while
    let mut counter = 0;

    let result = loop {
        counter += 1;

        if counter == 10 {
            break counter * 2;
        }
    };

    for element in a.iter() {
        println!("the value is: {}", element);
    };

    for number in (1..4).rev() {
        println!("{}!", number);
    }
}
