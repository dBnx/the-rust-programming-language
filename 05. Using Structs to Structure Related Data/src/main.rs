
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}

fn build_user(email: &str, username: &str) -> User {
    let email = String::from( email );
    let username = String::from( username );
    User {
        email, // Name of struct = name of var => shorthand notation
        username,
        active: true,
        sign_in_count: 1,
    }
}

fn build_user_alike(email: &str, username: &str, user_alike: User) -> User {
    User {
        email: String::from( email ),
        username: String::from( username ),
        ..user_alike // copies other fields
    }
}

fn main() {
    println!("Hello, world!");

    let user1 = User {
        email: String::from("someone@example.com"),
        username: String::from("someusername123"),
        active: true,
        sign_in_count: 1,
    };

    let mut user2 = build_user("test@mail.com", "Josef");
    //let mut user3 = build_user_alike("test3@mail.com", "Sarah", user1);

    // tuple structs
    struct Color(i32, i32, i32);
    struct Point(i32, i32, i32);

    let black = Color(0, 0, 0);
    let origin = Point(0, 0, 0);

    // unit-like structs because they behave similarly to (), the unit type
    // struct without any fields
    
    example();
}

#[derive(Debug)]
struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle{
    fn area( &self ) -> u32 {
        self.width * self.height
    }

    fn can_hold( &self, other: &Rectangle ) -> bool {
        self.width > other.width && self.height > other.height
    }
    
    // associated functions - don't use self
    fn square( side_length: u32 ) -> Rectangle {
        Rectangle{
            width: side_length,
            height: side_length,
        }
    }
}

fn example() {
    let rec1 = Rectangle{ width: 100, height: 50 };
    let rec2 = Rectangle{ width:  90, height: 40 };
    let rec3 = Rectangle{ width: 110, height: 40 };
    // {} uses Display trait by default, which is not implemented 
    // {:?} uses the Debug trait, which, when implemented, shows all fields
    // {:#?} is the same, but more verbose using multiple lines
    println!("rect1 is {:#?}", rec1); 
    println!("area of rect1 is {}", rec1.area() ); 
    println!("rect2 is {:?}", rec2); 
    println!("rect3 is {:?}", rec3); 
    println!("fits rec2 in rect1 : {}", rec1.can_hold( &rec2 ) ); 
    println!("fits rec3 in rect1 : {}", rec1.can_hold( &rec3 ) ); 

    let square = Rectangle::square( rec1.height );
    println!("square is {:#?}", square ); 
}
