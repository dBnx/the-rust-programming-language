
use automated_tests;

// """We create a tests directory at the top level of our project directory, next to src. 
// Cargo knows to look for integration test files in this directory. We can then make as 
// many test files as we want to in this directory, and Cargo will compile each of the files as 
// an individual crate."""
// => Common functionality can't be extracted to a seperate file in the tests folder
// => Alternative naming convention: common/mod.rs instead of common.rs

mod common;

#[test]
fn simple_test_of_public_interface() {
    common::setup();
    automated_tests::super_important_function();
}

// # Integration Tests for Binary Crates
// 
// If our project is a binary crate that only contains a src/main.rs file and doesn’t have a 
// src/lib.rs file, we can’t create integration tests in the tests directory and bring functions 
// defined in the src/main.rs file into scope with a use statement. Only library crates expose 
// functions that other crates can use; binary crates are meant to be run on their own.
// 
// This is one of the reasons Rust projects that provide a binary have a straightforward 
// src/main.rs file that calls logic that lives in the src/lib.rs file. Using that structure, 
// integration tests can test the library crate with use to make the important functionality 
// available. If the important functionality works, the small amount of code in the src/main.rs 
// file will work as well, and that small amount of code doesn’t need to be tested.
