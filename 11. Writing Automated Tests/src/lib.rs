
// Functions for testing:
// - assert!
// - assert_eq!
// - assert_ne!
// Last two compare the expected and actual value. The first one checks the boolean value
// without printing the value - all support adding a fmt string with arguments to print 
// additional informations.
// Types must implement PartialEq and Debug traits: 
//   #[derive(PartialEq, Debug)]

/// Returns 0 as an i64
///
/// # Examples
///
/// ```
/// let arg = 5;
/// let answer = automated_tests::super_important_function();
///
/// assert_eq!( 0, answer );
/// ```
pub fn super_important_function() -> i64 {
    0
}

#[derive(Debug)]
pub struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    pub fn can_hold(&self, other: &Rectangle) -> bool {
        self.width > other.width && self.height > other.height
    }
}

#[cfg(test)]
mod tests {

    #[test]
    fn it_works() {
        assert_eq!( 2 + 2, 4 );
    }

    #[test]
    #[ignore]
    fn should_fail() {
        assert_eq!( 0, 1 );
    }
    
    use super::*;

    #[test]
    fn larger_can_hold_smaller() {
        let larger = Rectangle {
            width: 8,
            height: 7,
        };
        let smaller = Rectangle {
            width: 5,
            height: 1,
        };

        assert!(larger.can_hold(&smaller));
    }

    #[test]
    fn smaller_cannot_hold_larger() {
        let larger = Rectangle {
            width: 8,
            height: 7,
        };
        let smaller = Rectangle {
            width: 5,
            height: 1,
        };

        assert!(!smaller.can_hold(&larger));
    }

    #[test]
    #[should_panic]
    fn should_panic() {
        panic!("AHHHHH");
    }

    #[test]
    #[should_panic(expected = "this is a substring")]
    fn should_panic_with_a_specific_message() {
        panic!("this is a substring of a possibly dynamically generated message");
    }
    
    #[test]
    fn returns_result() -> Result<(), String> {
        Ok(())
    }
    
}
