pub struct Cacher<T, A, B>
where
    T: Fn(A) -> B,
    A: Copy + std::cmp::PartialEq,
    B: Copy,
{
    closure: T,
    input: Option<A>,
    output: Option<B>,
}

impl<T, A, B> Cacher<T, A, B>
where
    T: Fn(A) -> B,
    A: Copy + std::cmp::PartialEq,
    B: Copy,
{
    pub fn new(closure: T) -> Cacher<T, A, B> {
        Cacher {
            closure,
            input: None,
            output: None,
        }
    }

    pub fn value(&mut self, arg: A) -> B {
        if self.output.is_some() && self.input.unwrap() == arg {
            return self.output.unwrap();
        }

        let v = (self.closure)(arg);
        self.input = Some(arg);
        self.output = Some(v);
        v
    }
}
