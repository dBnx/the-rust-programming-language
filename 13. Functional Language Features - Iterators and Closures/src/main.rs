use std::thread;
use std::time::Duration;

fn main() {
    println!("\n### main_closures ###" );
    main_closures();
    println!("\n### main_iterators ###" );
    main_iterators();
}

mod cacher;

// Closures
fn main_closures() {
    // All identical:
    fn add_one_v1(x: u32) -> u32 {
        x + 1
    }
    let add_one_v2 = |x: u32| -> u32 { x + 1 };
    let add_one_v3 = |x| x + 1;
    let add_one_v4 = |x| x + 1;
    add_one_v1(0u32); //        To remove unused warning
    add_one_v2(0u32); //        To remove unused warning
    add_one_v3(0u32); // Needed to infer type of x
    add_one_v4(0u32); // Needed to infer type of x

    let example_closure = |x| x;
    let _ = example_closure(String::from("hello"));
    //let n = example_closure(5); // error

    // Example where it is useful
    let expensive_function = |x| {
        println!("Expensive calculation ...");
        thread::sleep(Duration::from_secs(1));
        x
    };
    let mut lazy_res = cacher::Cacher::new(expensive_function);
    eprintln!("{:#?}", lazy_res.value(2));
    eprintln!("{:#?}", lazy_res.value(2));
    eprintln!("{:#?}", lazy_res.value(4));
    eprintln!("{:#?}", lazy_res.value(4));

    // Captures
    let x = 4;
    let equal_to_x = |z| z == x;
    let y = 4;
    assert!(equal_to_x(y));

    // Closures can capture values from their environment in three ways, which directly map to
    // the three ways a function can take a parameter: taking ownership, borrowing mutably, and
    // borrowing immutably. These are encoded in the three Fn traits as follows:
    // - FnOnce consumes the variables it captures from its enclosing scope, known as the
    //   closure’s environment. To consume the captured variables, the closure must take
    //   ownership of these variables and move them into the closure when it is defined.
    //   The Once part of the name represents the fact that the closure can’t take ownership
    //   of the same variables more than once, so it can be called only once.
    // - FnMut can change the environment because it mutably borrows values.
    // - Fn borrows values from the environment immutably.
}

mod counter;
use counter::Counter;

fn main_iterators() {
    let mut counter = Counter::new();
    counter.next();
    let squared_product: u32 = Counter::new()
        .zip(Counter::new())
        .map(|(x, y)| x * y)
        .product();
    eprintln!("squared_product = {:#?}", squared_product);

    // search implementation using iterator adapters:
    fn search<'a>(query: &str, contents: &'a str) -> Vec<&'a str> {
        contents
            .lines()
            .filter(|line| line.contains(query))
            .collect()
    }

    // TODO: Add everything after 13.2 or even earlier
}
