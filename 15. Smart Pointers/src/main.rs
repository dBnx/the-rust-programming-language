// 
// - Box<T> for allocating values on the heap
// - Rc<T>, a reference counting type that enables multiple ownership
// - Ref<T> and RefMut<T>, accessed through RefCell<T>, a type that enforces the borrowing rules
//   at runtime instead of compile time


fn main() {
    println!("Hello, world!");
    main_box();
}

mod cons_list;
use cons_list::List::{Cons, Nil};

fn main_box() {
    // Boxes can also be used for trait objects, where it points to smth that
    // implements a specific trait, but the type is of no importance 
    // e.g. Box<dyn Error>
    let mut ptr = Box::new( 0i32 );
    eprintln!( "ptr = {}", ptr );
    *ptr = 1;
    eprintln!( "ptr = {}", ptr );

    let _list = Cons( 1, Box::new(Cons( 2, Box::new(Cons( 3, Box::new(Nil) )) )) );

    // deref coercion
    fn hello(name: &str) {
        println!("Hello, {}!", name);
    }

    let m = Box::new(String::from("Rust"));
    hello(&m);
    // Box<T>::deref() returns a &T 
    // => If a type e.g. &Box<T> is given to a function requiring &T it is automatically reduced to it.
    // e.g. &Box<String> -> &String -> &str

    // Rust does deref coercion when it finds types and trait implementations in three cases:
    // - From &T to &U when T: Deref<Target=U>
    // - From &mut T to &mut U when T: DerefMut<Target=U>
    // - From &mut T to &U when T: Deref<Target=U>

}
