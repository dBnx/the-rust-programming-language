
pub enum List {
    Cons(i32, Box<List>),
    Nil,
}

impl<'a> Iterator for List {
    type Item = &'a List;

    fn next( &mut self ) -> Option<Self::Item> {
        match self {
            List::Cons(v, l) => Some(l),
            List::Nil => None,
        }
    }
}
