use std::borrow::Cow;

fn get_embedded_string() -> Cow<'static, str> {
    let bytes = include_bytes!("../resources/embed.txt");
    String::from_utf8_lossy(bytes)
}

pub fn main() {
    let embed = get_embedded_string();
    println!("Embedded: {}", embed);
}
