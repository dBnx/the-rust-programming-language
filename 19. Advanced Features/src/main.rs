mod functions_and_closures;
mod macros;

fn main() {
    functions_and_closures::main();
    macros::main();
}
