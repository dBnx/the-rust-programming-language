
pub fn main() {
    println!( "\n### Advanced Functions and Closures ###\n" );
    println!( "\
Functions Pointers:
Functions coerce to the type 'fn' ( not the 'Fn' closure trait )
e.g. fn(i32) -> i32
fn impelements all closure types: Fn FnMut FnOnce 
=> It's best to use closure types to accept closures and function pointers
   and only use fn if it interfaces with C

Closures don't have a specific type and closure trait types are unsized and need to be Box<>'d if they should be returned:
e.g. Box<dyn Fn(i32) -> i32>
" );

    fn add_one( x: i32 ) -> i32 {
        x + 1
    }

    fn apply_twice( f: fn(i32) -> i32, arg: i32 ) -> i32 {
        f( f( arg ) )
    }

    let answer = apply_twice( add_one, 0 );
    println!( "answer = apply_twice( add_one, 0 ) = {}\n", answer );

    
    let list_of_numbers = vec![1, 2, 3];
    // Convert Vec<i32> to Vec<String> ...
    // ... using an closure
    let _list_of_strings: Vec<String> = list_of_numbers.iter().map(|i| i.to_string()).collect();
    // ... using a  function pointer
    let _list_of_strings: Vec<String> = list_of_numbers.iter().map(ToString::to_string).collect();

    // Status::Value is a function (pointer) => it's possible to map values into enum's 
    enum Status {
        Value(u32),
        Stop,
    }

    let _list_of_statuses: Vec<Status> = (0u32..20).map(Status::Value).collect();
    let _ = Status::Stop;

    fn return_closure() -> Box< dyn Fn(i32) -> i32 > {
        Box::new( |x| x )
    }

    let _ = return_closure()(0);
    
}
