extern crate proc_macro;

use proc_macro::TokenStream; 
// comes with Rust, so we didn’t need to add that to the dependencies in Cargo.toml. The proc_macro crate is the compiler’s API that allows us to read and manipulate Rust code from our code.
use quote::quote;
// turns syn data structures back into Rust code. 
use syn;
// parses Rust code from a string into a data structure that we can perform operations on. 

#[proc_macro_derive(HelloMacro)]
pub fn hello_macro_derive(input: TokenStream) -> TokenStream {
    // Construct a representation of Rust code as a syntax tree
    // that we can manipulate
    let ast = syn::parse(input).unwrap();

    // Build the trait implementation
    impl_hello_macro(&ast)
}

fn impl_hello_macro(ast: &syn::DeriveInput) -> TokenStream {
    let name = &ast.ident;
    let gen = quote! { // quote! resturns ast of the code within
        impl HelloMacro for #name {
            fn hello_macro() {
                // quote! replaces #var with the value of var
                // stringify! is a Rust builtin, which stringifies the expression given
                // e.g. 1 + 2 => "1 + 2"
                println!("Hello, Macro! My name is {}!", stringify!(#name)); 
            }
        }
    };
    gen.into() // converts the ast back to a TokenStream
}

// Example syn::DeriveInput for a simple struct:
//   DeriveInput {
//       // --snip--
//   
//       ident: Ident {
//           ident: "Pancakes",
//           span: #0 bytes(95..103)
//       },
//       data: Struct(
//           DataStruct {
//               struct_token: Struct,
//               fields: Unit,
//               semi_token: Some(
//                   Semi
//               )
//           }
//       )
//   }
