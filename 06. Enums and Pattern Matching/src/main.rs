// Enums are a feature in many languages, but their capabilities differ in each language. 
// Rust’s enums are most similar to algebraic data types in functional languages, 
// such as F#, OCaml, and Haskell.

// Enum with associated data
enum IpAddr {
    V4(u8, u8, u8, u8),
    V4Str(String),
    V6(String),
}

#[derive(Debug)]
enum Message {
    Quit,
    Move { x: i32, y: i32 }, // anonymous struct
    Write(String),
    ChangeColor(i32, i32, i32),
}

// It is even possible to implement functions
impl Message {
    fn print( &self ) {
        println!("{:#?}", self)
    }
}

fn main() {
    let home = IpAddr::V4Str(String::from("127.0.0.1"));
    let home = IpAddr::V4(127,0,0,1);
    let loopback = IpAddr::V6(String::from("::1"));

    Message::ChangeColor(1,2,3).print();
    Message::Write( String::from("Test") ).print();

    let some_number = Some(5);
    let some_string = Some("a string");
    let absent_number: Option<i32> = None;

    // if we only want to handle one case: if let
    let some_u8_value = Some(0u8);
    if let Some(3) = some_u8_value {
        println!("three");
    }

    example();
}

// The option enum - defined in prelude and does not have to be brought into scope
//enum Option<T> {
//    Some(T),
//    None,
//}

#[derive(Debug)]
enum Currency {
    Bronze(f64),
    Silver(f64),
    Gold(f64),
    Platinum(f64),
}

fn convert_to_lower_type( currency: Currency ) -> Currency {
    match currency {
        Currency::Bronze(v)   => Currency::Bronze(         v ),
        Currency::Silver(v)   => Currency::Bronze( 100.0 * v ),
        Currency::Gold(v)     => Currency::Silver( 100.0 * v ),
        Currency::Platinum(v) => Currency::Gold(   100.0 * v ),
    }
}

fn convert_to_bronze( currency: Currency ) -> Currency {
    match currency {
        Currency::Bronze(v) => Currency::Bronze(         v ),
                          _ => convert_to_bronze( convert_to_lower_type( currency ) ),
    }
}

fn example(){
    let wealth = Currency::Platinum( 1.0 );
    println!("Wealth = {:#?}", wealth );
    let wealth = convert_to_lower_type( wealth );
    println!("Wealth = {:#?}", wealth );
    let wealth = convert_to_bronze( wealth );
    println!("Wealth = {:#?}", wealth );
}
