use rand::Rng; // Defines traits for rng

const SECRET_NUMBER_MAX : i32 = 2;

fn main() {
    println!("Please guess my secret number, which is in the range [ 1, {} ]:", SECRET_NUMBER_MAX);

    let secret_number = rand::thread_rng().gen_range(1..(SECRET_NUMBER_MAX+1));

    loop {

        let mut guess = String::new();
        std::io::stdin().read_line( &mut guess).expect("Failed to read line.");
        
        let guess :i32 = match guess.trim().parse() {
                Ok( guess ) => guess,
                Err( _ ) => continue,
        };

        match guess.cmp( &secret_number )  {
            std::cmp::Ordering::Less => println!("Too small!"),
            std::cmp::Ordering::Greater => println!("Too big!"),
            std::cmp::Ordering::Equal => { println!("Just right!"); break; },
        };
    }
}
