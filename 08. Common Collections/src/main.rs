
// A vector allows you to store a variable number of values next to each other.
// A string is a collection of characters. We’ve mentioned the String type previously, 
//   but in this chapter we’ll talk about it in depth.
// A hash map allows you to associate a value with a particular key. It’s a particular 
//   implementation of the more general data structure called a map.
fn main() {
    println!("\n === Vectors === \n");
    vectors();

    println!("\n === Strings === \n");
    strings();

    println!("\n === Hashmaps === \n");
    hashmaps();
}

fn vectors() {
    let v: Vec<i32> = Vec::new();
    let mut v = vec![1, 2, 3]; // Using initial values and type detuction
    v.push(4);
    v.push(5);
    v.push(6);
    
    // References
    let v = vec![1, 2, 3, 4, 5];

    // panic if error
    let third: &i32 = &v[2];
    // v.push(5); // won't work, as there is a reference into v and it would create a mut. borrow
    println!("The third element is {}", third);

    // returns Option<T>
    match v.get(2) {
        Some(third) => println!("The third element is {}", third),
        None => println!("There is no third element."),
    }

    // Loop over
    let v = vec![100, 32, 57];
    for i in &v {
        print!("{} ", i);
    }
    println!("");

    let mut v = vec![100, 32, 57];
    for i in &mut v {
        *i += 50;
    }

    // Example spreadsheet
    enum SpreadsheetCell {
        Int(i32),
        Float(f64),
        Text(String),
    }

    let row = vec![
        SpreadsheetCell::Int(3),
        SpreadsheetCell::Text(String::from("blue")),
        SpreadsheetCell::Float(10.12),
    ];
}

fn strings() {
    // .to_string(); is implemented for every type with the Display trait:
    let mut s = "foo".to_string();
    // Same as: let mut s = String::from("foo");
    
    s.push(' ');
    s.push_str("bar"); // Does not take ownership

    // Concatenation
    let s1 = String::from("Hello, ");
    let s2 = String::from("world!");
    let s3 = s1 + &s2; // Move of s1: + calls >fn add(self, s: &str) -> String 
    // &s2 gives a &String, which the compiler can coerce to a &str: 
    // &s2 =[deref coercion]=> &s2[..]
    // Also note the self (not &self) in add, which takes owenership 

    let s1 = String::from("tic");
    let s2 = String::from("tac");
    let s3 = String::from("toe");
    let s = s1 + "-" + &s2 + "-" + &s3;

    let s1 = String::from("tic");
    let s = format!("{}-{}-{}", s1, s2, s3); // Doesn't take ownership!

    // Indexing into Strings
    // String wraps Vec<u8>
    let hello = String::from("Здравствуйте");
    // let answer = &hello[0]; Does not make sense in an UTF-8 string
    
    // Text:
    //  नमस्ते
    // Bytes: 
    //  [224, 164, 168, 224, 164, 174, 224, 164, 184, 224, 165, 141, 224, 164, 164, 224, 165, 135]
    // Unicode scalar values ( what rust understands as chars ):
    //  ['न', 'म', 'स', '्', 'त', 'े']
    // Grapheme clusters:
    //  ["न", "म", "स्", "ते"]
    //  The 4. and 6. unicode scalar ( char ) are diacritics.
    let hello = "Здравствуйте";
    let s = &hello[0..4]; // bytes 
    println!( "{} -> {}", hello, s );
    // let s = &hello[0..1]; // panic! First character is 2 bytes 
    
    // Loops over chars ( Unicode scalars )
    for c in "नमस्ते".chars() { // 6 chars with 2 diacritics
        print!("{} ", c);
    }
    println!("");

    for b in "नमस्ते".bytes() { // 18 bytes
        print!("{} ", b);
    }
    println!("");

    // Loop over grapheme clusters is not in the std lib
}


fn hashmaps() {
    use std::collections::HashMap;

    // Type is inferred from inserts
    let mut map = HashMap::new(); 
    map.insert("a".to_string(), 10);
    map.insert("b".to_string(), 10);
    map.insert("c".to_string(), 10);

    // Construct using .collect()
    let teams = vec![String::from("Blue"), String::from("Yellow")];
    let initial_scores = vec![10, 50];

    let mut scores: HashMap<_, _> =
        teams.into_iter().zip(initial_scores.into_iter()).collect();

    // Inserting
    let field_name = String::from("d");
    let field_value = 13;
    // moves field_name and copies field_value, because of the Copy trait
    map.insert(field_name, field_value); 

    // Accessing Values in a Hash Map
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    scores.insert(String::from("Yellow"), 50);

    let team_name = String::from("Blue");
    let score1 = scores.get(&team_name);
    let score2 = scores.get("Blue");
    assert_eq!( score1, score2 );

    for (key, value) in &scores {
        print!("{}: {}", key, value);
    }  
    println!("");

    // Updating values
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);
    println!("{:?}", scores);
    scores.insert(String::from("Blue"), 25); // Will replace the first value
    println!("{:?}", scores);

    scores.entry(String::from("Yellow")).or_insert(50);
    scores.entry(String::from("Blue")).or_insert(50);
    println!("{:?}", scores);

    println!("{:?}", scores);

    // Insert if it doesn't exist using .entry() - which returns an Enum 'Entry'
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10);

    let tmp = scores.entry(String::from("Yellow"));  
    tmp.or_insert(50); // changes scores, but tmp is not mutable?
    scores.entry(String::from("Blue")).or_insert(50); // does nothing, as Blue exists

    println!("{:?}", scores);

    // Updated based on prev. value:
    let text = "hello world wonderful world";
    let mut map = HashMap::new();

    for word in text.split_whitespace() {
        let count = map.entry(word).or_insert(0);
        *count += 1;
    }
    println!("{:?}", map);
    
    // By default SipHash is used. Moderate speed to prevent DOS attacks.
    // It can use anything which implements the BuildHasher trait.
}
