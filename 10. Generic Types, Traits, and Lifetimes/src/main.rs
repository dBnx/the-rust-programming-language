
use std::fmt::Debug;

fn main() {
    println!("\n\n === generics === ");
    generics();
    println!("\n\n === traits === ");
    traits(); 
    println!("\n\n === lifetimes === ");
    lifetimes();
}

// Generic function 
// Compare: fn largest( list: &[i32] ) -> i32 {
fn largest<T: Copy + std::cmp::PartialOrd>( list: &[T] ) -> T {
    let mut largest = list[0];
    for &item in list {
        if item > largest {
            largest = item;
        }
    }
    largest
}

// Generic struct
struct Point3<T> {
    x: T,
    y: T,
    z: T,
}
// All T's
impl<T> Point3<T> {
    fn get_x(&self) -> &T {
        &self.x
    }
}
// Only for some T's
impl<T: std::cmp::PartialEq> Point3<T> {
    fn are_all_equal(&self) -> bool {
        self.x == self.y && self.y == self.z
    }
}
// Only for T = f32
impl Point3<f32> {
    fn distance_from_origin(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2) + self.z.powi(2)).sqrt()
    }
}

// Inhomogious
struct Point3_inhom<T, U, V> {
    x: T,
    y: U,
    z: V,
}

impl<T, U, V> Point3_inhom<T, U, V> {
    fn mixup<M, N, O>(self, other: Point3_inhom<M, N, O>) -> Point3_inhom<T, N, O> {
        Point3_inhom {
            x: self.x,
            y: other.y,
            z: other.z,
        }
    }
}
// Enums
enum Option<T> {
    Some(T),
    None,
}

enum Result<T, E> {
    Ok(T),
    Err(E),
}

fn generics() {
    let number_list = vec![34, 50, 25, 100, 65];
    let number_list_max = largest( &number_list );
    println!("The largest number is {}", number_list_max);

    let _integers = Point3{ x: 1, y: 2, z: 3 };
    let _floats = Point3{ x: 1.0, y: 2.0, z: 3.0 };
    _integers.get_x();
    _floats.are_all_equal();

    let _integers = Point3_inhom{ x: 1, y: 2, z: 3 };
    let _floats = Point3_inhom{ x: 1, y: 'a', z: 3.0 };
    _integers.mixup( _floats );
}

pub trait Summary {

    // Default implementation:
    fn summarize_author(&self) -> String { 
        "(Unknown)".to_string()
    }

    // Without impelmentation:
    // fn summarize(&self) -> String;
    // Using trait functions
    fn summarize(&self) -> String {
        format!("(Read more from {}...)", self.summarize_author())
    }
}

pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}

impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{}, by {} ({})", self.headline, self.author, self.location)
    }
}

pub struct Tweet {
    pub username: String,
    pub content: String,
    pub reply: bool,
    pub retweet: bool,
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}

// Trait as parameters: 
// Same as: pub fn notify<T: Summary>(item: &T)
pub fn notify(item: &impl Summary) {
    println!("Breaking news! {}", item.summarize());
}

// Use multiple traits using the syntax sugar:
// Same as: pub fn notify_and_display<T: Summary + Display>(item: &T) 
pub fn notify_and_display(item: &(impl Summary + std::fmt::Display)) {
    println!("Breaking news! {}. {}", item.summarize(), item);
}


// Complax trait bounds using the where clause:
// Same as: fn some_function<T: Display + Clone, U: Clone + Debug>(t: &T, u: &U) -> i32
fn some_function<T, U>(_t: &T, _u: &U) -> i32
    where T: std::fmt::Display + Clone,
          U: Clone + Debug
{
    0
}

// Returning a trait type:
fn returns_summarizable() -> impl Summary {
    Tweet {
        username: String::from("horse_ebooks"),
        content: String::from(
            "of course, as you probably already know, people",
        ),
        reply: false,
        retweet: false,
    }
}

fn traits() {
    let tweet = Tweet {
        username: String::from("horse_ebooks"),
        content: String::from(
            "of course, as you probably already know, people",
        ),
        reply: false,
        retweet: false,
    };

    println!("1 new tweet: {}", tweet.summarize());       
}

fn lifetimes() {
}
