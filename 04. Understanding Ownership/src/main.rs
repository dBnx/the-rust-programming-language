
fn main() {
    println!("Hello, world!");

    let s1 = String::from("hello");
    let s2 = s1; // Move! s1 is now invalid

    let x = 3;
    let y = x; // Copy, because x implements the Copy trait / inexpensive and lives on the stack

    // In addition, there’s a design choice that’s implied by this: 
    // > Rust will never automatically create “deep” copies of your data. 
    // Therefore, any automatic copying can be assumed to be inexpensive in terms 
    // of runtime performance.
    
    let s1 = String::from("hello");
    let s2 = s1.clone();
    println!("s1 = {}, s2 = {}", s1, s2);

    // Borrow: Multiple immutable references are allowed
    let s1 = String::from("hello");
    let len = calculate_length(&s1);
    println!("The length of '{}' is {}.", s1, len);

    // Mutable borrow: At any time there can only be one mutable ref. - not even other immut. refs!
    let mut s = String::from("hello");
    change(&mut s);

    // Note that a reference’s scope starts from where it is introduced and continues through the 
    // last time that reference is used. For instance, this code will compile because the last 
    // usage of the immutable references occurs before the mutable reference is introduced:
    let mut s = String::from("hello");

    let r1 = &s; // no problem
    let r2 = &s; // no problem
    println!("{} and {}", r1, r2);
    // r1 and r2 are no longer used after this point

    let r3 = &mut s; // no problem
    println!("{}", r3);

    // Slices:
    let s = String::from("hello world");
    let hello = &s[0..5]; 
    let world = &s[6..11];

    let mut s = String::from("hello world");
    let word = first_word(&s);
    // if s gets invalid/changed -> word is invalidated too 
    // because there can only exist one mutable reference :)
    
    //String Literals Are Slices
    let a = [1, 2, 3, 4, 5];
    let slice = &a[1..3];  // Type: &[i32]
}

fn calculate_length(s: &String) -> usize { 
    s.len()
} 

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}

// String slices: str
// Should be: fn first_word(s: &str) -> &str {
fn first_word(s: &String) -> &str {
    let bytes = s.as_bytes();

    for (i, &item) in bytes.iter().enumerate() {
        if item == b' ' {
            return &s[0..i];
        }
    }

    &s[..]
}
