use std::error::Error;

// It’s advisable to have your code panic when it’s possible that your code could end up in a 
// bad state. In this context, a bad state is when some assumption, guarantee, contract, 
// or invariant has been broken, such as when invalid values, contradictory values, or 
// missing values are passed to your code—plus one or more of the following:
// 
// > The bad state is not something that’s expected to happen occasionally.
// > Your code after this point needs to rely on not being in this bad state.
// > There’s not a good way to encode this information in the types you use.

fn main() -> Result<(), Box<dyn Error>> {
    unrecoverable_errors();
    recoverable_errors()?;
    custom_type();
    Ok( () )
}

fn recoverable_errors() -> Result<(), Box<dyn Error>>{
    //enum Result<T, E> {
    //    Ok(T),
    //    Err(E),
    //}
    use std::fs::File;

    // Using a simple match.
    //let f = File::open("hello.txt");
    //let f = match f {
    //    Ok( file ) => file,
    //    Err( _ ) => panic!("There exists no hello.txt!"),
    //};
    
    // Cascade of matches
    {
        let filename = "hello.txt";
        let f = match File::open( filename ) {
            Ok( file ) => file,
            Err( error ) => match error.kind() {
                std::io::ErrorKind::NotFound => match File::create( filename ) {
                    Ok( f ) => f,
                    Err( e ) => panic!("Problem creating the file: {:?}", e),
                },
                other_error => panic!("Error accesing file {}. Got: {:?}", filename, other_error),
            },
        };
    }

    // Alternative using closures
    {
        let f = File::open("hello.txt").unwrap_or_else(|error| {
            if error.kind() == std::io::ErrorKind::NotFound {
                File::create("hello.txt").unwrap_or_else(|error| {
                    panic!("Problem creating the file: {:?}", error);
                })
            } else {
                panic!("Problem opening the file: {:?}", error);
            }
        });
    }
    {   // Same as the first example: Get T and panic! otherwise.
        let f = File::open("hello.txt").unwrap();
    }
    {   // Same, but with a custom panic! message.
        let f = File::open("hello.txt").expect("Failed to open hello.txt");
    }

    // Error propagation:
    fn read_file_to_string_verbose( filename: &str) -> Result<String, std::io::Error > {
        use std::io::Read;
        use std::fs::File;

        let mut f = match File::open( filename ) {
            Ok( file ) => file,
            Err( error ) => return Err( error ),
        };

        let mut line = String::new();
        match f.read_to_string( &mut line ) {
            Ok( _ ) => Ok( line ),
            Err( e ) => Err( e )
        }
    }
    // Can be done more nicely using the ? operator, which almost the same:
    fn read_file_to_string( filename: &str) -> Result<String, std::io::Error > {
        use std::io::Read;
        use std::fs::File;

        let mut f = File::open( filename )?;
        let mut line = String::new();
        f.read_to_string( &mut line )?;
        Ok(line)
    }
    // Difference: ? calls the from function from the From trait to convert the error type
    // to the one specified in the return type. It can be used in every function that
    // returns Result<T,E>, Option<T> or a type which implements std::ops::Try.
    fn read_file_to_string_even_smaller() -> Result<String, std::io::Error> {
        use std::io::Read;
        let mut s = String::new();
        File::open("hello.txt")?.read_to_string(&mut s)?;
        Ok(s)
    }
    // The functions just do, what is already in the std:
    // std::fs::read_to_string("hello.txt")
    
    // main is allowed to return () or Result<(), Box<dyn std::error::Error>>
    //  fn main() -> Result<(), Box<dyn Error>> {
    // Box<dyn Error> is a trait type -> Ch.17
    Ok(())
}


fn unrecoverable_errors() {
    // Unrecoverable errors:
    // panic!("Test");
    let _v = vec![1, 2, 3];
    // _v[99]; causes:
    // 0: rust_begin_unwind
    //          at /rustc/.../library/std/src/panicking.rs:493:5
    // 1: core::panicking::panic_fmt
    //          at /rustc/.../library/core/src/panicking.rs:92:14
    // 2: core::panicking::panic_bounds_check
    //          at /rustc/.../library/core/src/panicking.rs:69:5
    // 3: <usize as core::slice::index::SliceIndex<[T]>>::index
    //          at /home/dave/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/library/core/src/slice/index.rs:182:10
    // 4: core::slice::index::<impl core::ops::index::Index<I> for [T]>::index
    //          at /home/dave/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/library/core/src/slice/index.rs:15:9
    // 5: <alloc::vec::Vec<T,A> as core::ops::index::Index<I>>::index
    //          at /home/dave/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/library/alloc/src/vec/mod.rs:2381:9
    // 6: error_handling::main
    //          at ./src/main.rs:6:5
    // 7: core::ops::function::FnOnce::call_once
    //         at /home/dave/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/lib/rustlib/src/rust/library/core/src/ops/function.rs:227:5
    //note: Some details are omitted, run with `RUST_BACKTRACE=full` for a verbose backtrace.

}

// Corresponding to the example in Ch.2
pub struct Guess {
    value: i32,
}

impl Guess {
    pub fn new(value: i32) -> Guess {
        if value < 1 || value > 100 {
            panic!("Guess value must be between 1 and 100, got {}.", value);
        }

        Guess { value }
    }

    pub fn value(&self) -> i32 {
        self.value
    }
}

fn custom_type() {
    let guess = Guess::new( 1 ); 
    guess.value(); // Definitely is a valid guess.

    // Does not fulfill the contract 
    //let guess = Guess::new( -1 ); 
}
